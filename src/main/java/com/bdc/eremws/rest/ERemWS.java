package com.bdc.eremws.rest;

import com.bdc.eremws.wsdlclient.CreateRemittanceRequest;
import com.bdc.eremws.wsdlclient.CreateRemittanceResponse;
import com.bdc.eremws.wsdlclient.ValidateAccountRequest;
import com.bdc.eremws.wsdlclient.ValidateAccountResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ERemWS {
    @PostMapping("createACHRemittance")
    public CreateRemittanceResponse createACHRemittance(@RequestBody CreateRemittanceRequest createRemittanceRequest) {
        CreateRemittanceResponse res = new CreateRemittanceResponse();
        res.setRemittanceReferenceNumber(createRemittanceRequest.getUsername());
        return res;
    }

    @PostMapping("createBankDepositRemittance")
    public CreateRemittanceResponse createBankDepositRemittance(@RequestBody CreateRemittanceRequest createRemittanceRequest) {
        CreateRemittanceResponse res = new CreateRemittanceResponse();
        res.setRemittanceReferenceNumber(createRemittanceRequest.getUsername());
        return res;
    }

    @PostMapping("createWalletRemittance")
    public CreateRemittanceResponse createWalletRemittance(@RequestBody CreateRemittanceRequest createRemittanceRequest) {
        CreateRemittanceResponse res = new CreateRemittanceResponse();
        res.setRemittanceReferenceNumber(createRemittanceRequest.getUsername());
        return res;
    }

    @PostMapping("validateAccount")
    public ValidateAccountResponse validateAccount(@RequestBody ValidateAccountRequest validateAccountRequest) {
        ValidateAccountResponse res = new ValidateAccountResponse();
        //res.setRemittanceReferenceNumber(createRemittanceRequest.getUsername());
       // res.setAccountHolderName(validateAccountRequest.);
        return res;
    }
}

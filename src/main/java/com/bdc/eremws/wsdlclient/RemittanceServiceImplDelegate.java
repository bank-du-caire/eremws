
package com.bdc.eremws.wsdlclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "RemittanceServiceImplDelegate", targetNamespace = "http://services.asset.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface RemittanceServiceImplDelegate {


    /**
     * 
     * @param validateAccountRequest
     * @return
     *     returns com.bdc.riabankdeposit.client.ValidateAccountResponse
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://services.asset.com/RemittanceServiceImplDelegate/validateAccountRequest", output = "http://services.asset.com/RemittanceServiceImplDelegate/validateAccountResponse")
    public ValidateAccountResponse validateAccount(
        @WebParam(name = "validateAccountRequest", partName = "validateAccountRequest")
        ValidateAccountRequest validateAccountRequest);

    /**
     * 
     * @param createRemittanceRequest
     * @return
     *     returns com.bdc.riabankdeposit.client.CreateRemittanceResponse
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://services.asset.com/RemittanceServiceImplDelegate/createRemittanceRequest", output = "http://services.asset.com/RemittanceServiceImplDelegate/createRemittanceResponse")
    public CreateRemittanceResponse createRemittance(
        @WebParam(name = "createRemittanceRequest", partName = "createRemittanceRequest")
        CreateRemittanceRequest createRemittanceRequest);

    /**
     * 
     * @param enqiureRemittanceRequest
     * @return
     *     returns com.bdc.riabankdeposit.client.EnqiureRemittanceResponse
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://services.asset.com/RemittanceServiceImplDelegate/enqiureRemittanceRequest", output = "http://services.asset.com/RemittanceServiceImplDelegate/enqiureRemittanceResponse")
    public EnqiureRemittanceResponse enqiureRemittance(
        @WebParam(name = "enqiureRemittanceRequest", partName = "enqiureRemittanceRequest")
        EnqiureRemittanceRequest enqiureRemittanceRequest);

}

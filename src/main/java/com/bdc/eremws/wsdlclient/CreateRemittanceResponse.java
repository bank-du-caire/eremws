
package com.bdc.eremws.wsdlclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createRemittanceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createRemittanceResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.asset.com/}webServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="remittanceReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createRemittanceResponse", propOrder = {
    "remittanceReferenceNumber"
})
public class CreateRemittanceResponse
    extends WebServiceResponse
{

    protected String remittanceReferenceNumber;

    /**
     * Gets the value of the remittanceReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemittanceReferenceNumber() {
        return remittanceReferenceNumber;
    }

    /**
     * Sets the value of the remittanceReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemittanceReferenceNumber(String value) {
        this.remittanceReferenceNumber = value;
    }

}

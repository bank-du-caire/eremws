
package com.bdc.eremws.wsdlclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enqiureRemittanceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enqiureRemittanceRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.asset.com/}baseRequest">
 *       &lt;sequence>
 *         &lt;element name="remittanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enqiureRemittanceRequest", propOrder = {
    "remittanceID"
})
public class EnqiureRemittanceRequest
    extends BaseRequest
{

    protected String remittanceID;

    /**
     * Gets the value of the remittanceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemittanceID() {
        return remittanceID;
    }

    /**
     * Sets the value of the remittanceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemittanceID(String value) {
        this.remittanceID = value;
    }

}


package com.bdc.eremws.wsdlclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createRemittanceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createRemittanceRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.asset.com/}baseRequest">
 *       &lt;sequence>
 *         &lt;element name="remittance" type="{http://services.asset.com/}remittance" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createRemittanceRequest", propOrder = {
    "remittance"
})
public class CreateRemittanceRequest
    extends BaseRequest
{

    protected Remittance remittance;

    /**
     * Gets the value of the remittance property.
     * 
     * @return
     *     possible object is
     *     {@link Remittance }
     *     
     */
    public Remittance getRemittance() {
        return remittance;
    }

    /**
     * Sets the value of the remittance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Remittance }
     *     
     */
    public void setRemittance(Remittance value) {
        this.remittance = value;
    }

}


package com.bdc.eremws.wsdlclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enqiureRemittanceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enqiureRemittanceResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.asset.com/}webServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="remittanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="remittanceStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="remittanceStatusDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enqiureRemittanceResponse", propOrder = {
    "remittanceID",
    "remittanceStatusCode",
    "remittanceStatusDesc"
})
public class EnqiureRemittanceResponse
    extends WebServiceResponse
{

    protected String remittanceID;
    protected String remittanceStatusCode;
    protected String remittanceStatusDesc;

    /**
     * Gets the value of the remittanceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemittanceID() {
        return remittanceID;
    }

    /**
     * Sets the value of the remittanceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemittanceID(String value) {
        this.remittanceID = value;
    }

    /**
     * Gets the value of the remittanceStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemittanceStatusCode() {
        return remittanceStatusCode;
    }

    /**
     * Sets the value of the remittanceStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemittanceStatusCode(String value) {
        this.remittanceStatusCode = value;
    }

    /**
     * Gets the value of the remittanceStatusDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemittanceStatusDesc() {
        return remittanceStatusDesc;
    }

    /**
     * Sets the value of the remittanceStatusDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemittanceStatusDesc(String value) {
        this.remittanceStatusDesc = value;
    }

}

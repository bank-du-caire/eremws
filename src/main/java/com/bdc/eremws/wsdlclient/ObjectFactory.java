
package com.bdc.eremws.wsdlclient;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bdc.riabankdeposit.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bdc.riabankdeposit.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BaseRequest }
     * 
     */
    public BaseRequest createBaseRequest() {
        return new BaseRequest();
    }

    /**
     * Create an instance of {@link WebServiceResponse }
     * 
     */
    public WebServiceResponse createWebServiceResponse() {
        return new WebServiceResponse();
    }

    /**
     * Create an instance of {@link CreateRemittanceResponse }
     * 
     */
    public CreateRemittanceResponse createCreateRemittanceResponse() {
        return new CreateRemittanceResponse();
    }

    /**
     * Create an instance of {@link EnqiureRemittanceRequest }
     * 
     */
    public EnqiureRemittanceRequest createEnqiureRemittanceRequest() {
        return new EnqiureRemittanceRequest();
    }

    /**
     * Create an instance of {@link ValidateAccountResponse }
     * 
     */
    public ValidateAccountResponse createValidateAccountResponse() {
        return new ValidateAccountResponse();
    }

    /**
     * Create an instance of {@link EnqiureRemittanceResponse }
     * 
     */
    public EnqiureRemittanceResponse createEnqiureRemittanceResponse() {
        return new EnqiureRemittanceResponse();
    }

    /**
     * Create an instance of {@link Remittance }
     * 
     */
    public Remittance createRemittance() {
        return new Remittance();
    }

    /**
     * Create an instance of {@link ValidateAccountRequest }
     * 
     */
    public ValidateAccountRequest createValidateAccountRequest() {
        return new ValidateAccountRequest();
    }

    /**
     * Create an instance of {@link CreateRemittanceRequest }
     * 
     */
    public CreateRemittanceRequest createCreateRemittanceRequest() {
        return new CreateRemittanceRequest();
    }

}

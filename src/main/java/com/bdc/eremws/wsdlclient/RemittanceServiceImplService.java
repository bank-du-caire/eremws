
package com.bdc.eremws.wsdlclient;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "RemittanceServiceImplService", targetNamespace = "http://services.asset.com/", wsdlLocation = "file:/D:/Work/BVS/BDC/Projects/E-RemWSUtil/Docs/Eremittance_Test_wsdl/RemittanceServiceImplService.wsdl")
public class RemittanceServiceImplService
    extends Service
{

    private final static URL REMITTANCESERVICEIMPLSERVICE_WSDL_LOCATION;
    private final static WebServiceException REMITTANCESERVICEIMPLSERVICE_EXCEPTION;
    private final static QName REMITTANCESERVICEIMPLSERVICE_QNAME = new QName("http://services.asset.com/", "RemittanceServiceImplService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/D:/Work/BVS/BDC/Projects/E-RemWSUtil/Docs/Eremittance_Test_wsdl/RemittanceServiceImplService.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        REMITTANCESERVICEIMPLSERVICE_WSDL_LOCATION = url;
        REMITTANCESERVICEIMPLSERVICE_EXCEPTION = e;
    }

    public RemittanceServiceImplService() {
        super(__getWsdlLocation(), REMITTANCESERVICEIMPLSERVICE_QNAME);
    }

    public RemittanceServiceImplService(WebServiceFeature... features) {
        super(__getWsdlLocation(), REMITTANCESERVICEIMPLSERVICE_QNAME, features);
    }

    public RemittanceServiceImplService(URL wsdlLocation) {
        super(wsdlLocation, REMITTANCESERVICEIMPLSERVICE_QNAME);
    }

    public RemittanceServiceImplService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, REMITTANCESERVICEIMPLSERVICE_QNAME, features);
    }

    public RemittanceServiceImplService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public RemittanceServiceImplService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns RemittanceServiceImplDelegate
     */
    @WebEndpoint(name = "RemittanceServiceImplPort")
    public RemittanceServiceImplDelegate getRemittanceServiceImplPort() {
        return super.getPort(new QName("http://services.asset.com/", "RemittanceServiceImplPort"), RemittanceServiceImplDelegate.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns RemittanceServiceImplDelegate
     */
    @WebEndpoint(name = "RemittanceServiceImplPort")
    public RemittanceServiceImplDelegate getRemittanceServiceImplPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://services.asset.com/", "RemittanceServiceImplPort"), RemittanceServiceImplDelegate.class, features);
    }

    private static URL __getWsdlLocation() {
        if (REMITTANCESERVICEIMPLSERVICE_EXCEPTION!= null) {
            throw REMITTANCESERVICEIMPLSERVICE_EXCEPTION;
        }
        return REMITTANCESERVICEIMPLSERVICE_WSDL_LOCATION;
    }

}


package com.bdc.eremws.wsdlclient;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for remittance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="remittance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bdcBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bdcRefNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryBDCaccountNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryBirthdate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="beneficiaryCountry" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="beneficiaryCountryOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryNationalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryNationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryPassportNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deliveredCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="detailedPurpose" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="initiatedFromBranchCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="remittanceAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="remittanceRefNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderBirthdate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="senderCountry" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="senderCountryOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderMobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderNationalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderNationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderPassportNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="thirdPartyAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="thirdPartyBankCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="thirdPartyBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="companyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "remittance", propOrder = {
    "bdcBranchCode",
    "bdcRefNumber",
    "beneficiaryAddress",
    "beneficiaryBDCaccountNum",
    "beneficiaryBirthdate",
    "beneficiaryCountry",
    "beneficiaryCountryOfBirth",
    "beneficiaryMobile",
    "beneficiaryName",
    "beneficiaryNationalId",
    "beneficiaryNationality",
    "beneficiaryPassportNumber",
    "currencyCode",
    "deliveredCurrencyCode",
    "detailedPurpose",
    "initiatedFromBranchCode",
    "remittanceAmount",
    "remittanceRefNumber",
    "senderAddress",
    "senderBirthdate",
    "senderCountry",
    "senderCountryOfBirth",
    "senderMobileNumber",
    "senderName",
    "senderNationalId",
    "senderNationality",
    "senderPassportNumber",
    "thirdPartyAccountNumber",
    "thirdPartyBankCode",
    "thirdPartyBranchCode",
    "type",
    "companyCode"
})
public class Remittance {

    protected String bdcBranchCode;
    protected String bdcRefNumber;
    protected String beneficiaryAddress;
    protected String beneficiaryBDCaccountNum;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar beneficiaryBirthdate;
    protected int beneficiaryCountry;
    protected String beneficiaryCountryOfBirth;
    protected String beneficiaryMobile;
    protected String beneficiaryName;
    protected String beneficiaryNationalId;
    protected String beneficiaryNationality;
    protected String beneficiaryPassportNumber;
    protected String currencyCode;
    protected int deliveredCurrencyCode;
    protected String detailedPurpose;
    protected Integer initiatedFromBranchCode;
    protected BigDecimal remittanceAmount;
    protected String remittanceRefNumber;
    protected String senderAddress;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar senderBirthdate;
    protected int senderCountry;
    protected String senderCountryOfBirth;
    protected String senderMobileNumber;
    protected String senderName;
    protected String senderNationalId;
    protected String senderNationality;
    protected String senderPassportNumber;
    protected String thirdPartyAccountNumber;
    protected String thirdPartyBankCode;
    protected String thirdPartyBranchCode;
    protected String type;
    protected String companyCode;

    /**
     * Gets the value of the bdcBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBdcBranchCode() {
        return bdcBranchCode;
    }

    /**
     * Sets the value of the bdcBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBdcBranchCode(String value) {
        this.bdcBranchCode = value;
    }

    /**
     * Gets the value of the bdcRefNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBdcRefNumber() {
        return bdcRefNumber;
    }

    /**
     * Sets the value of the bdcRefNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBdcRefNumber(String value) {
        this.bdcRefNumber = value;
    }

    /**
     * Gets the value of the beneficiaryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    /**
     * Sets the value of the beneficiaryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryAddress(String value) {
        this.beneficiaryAddress = value;
    }

    /**
     * Gets the value of the beneficiaryBDCaccountNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryBDCaccountNum() {
        return beneficiaryBDCaccountNum;
    }

    /**
     * Sets the value of the beneficiaryBDCaccountNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryBDCaccountNum(String value) {
        this.beneficiaryBDCaccountNum = value;
    }

    /**
     * Gets the value of the beneficiaryBirthdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBeneficiaryBirthdate() {
        return beneficiaryBirthdate;
    }

    /**
     * Sets the value of the beneficiaryBirthdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBeneficiaryBirthdate(XMLGregorianCalendar value) {
        this.beneficiaryBirthdate = value;
    }

    /**
     * Gets the value of the beneficiaryCountry property.
     * 
     */
    public int getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    /**
     * Sets the value of the beneficiaryCountry property.
     * 
     */
    public void setBeneficiaryCountry(int value) {
        this.beneficiaryCountry = value;
    }

    /**
     * Gets the value of the beneficiaryCountryOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryCountryOfBirth() {
        return beneficiaryCountryOfBirth;
    }

    /**
     * Sets the value of the beneficiaryCountryOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryCountryOfBirth(String value) {
        this.beneficiaryCountryOfBirth = value;
    }

    /**
     * Gets the value of the beneficiaryMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryMobile() {
        return beneficiaryMobile;
    }

    /**
     * Sets the value of the beneficiaryMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryMobile(String value) {
        this.beneficiaryMobile = value;
    }

    /**
     * Gets the value of the beneficiaryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    /**
     * Sets the value of the beneficiaryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryName(String value) {
        this.beneficiaryName = value;
    }

    /**
     * Gets the value of the beneficiaryNationalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryNationalId() {
        return beneficiaryNationalId;
    }

    /**
     * Sets the value of the beneficiaryNationalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryNationalId(String value) {
        this.beneficiaryNationalId = value;
    }

    /**
     * Gets the value of the beneficiaryNationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryNationality() {
        return beneficiaryNationality;
    }

    /**
     * Sets the value of the beneficiaryNationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryNationality(String value) {
        this.beneficiaryNationality = value;
    }

    /**
     * Gets the value of the beneficiaryPassportNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryPassportNumber() {
        return beneficiaryPassportNumber;
    }

    /**
     * Sets the value of the beneficiaryPassportNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryPassportNumber(String value) {
        this.beneficiaryPassportNumber = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the deliveredCurrencyCode property.
     * 
     */
    public int getDeliveredCurrencyCode() {
        return deliveredCurrencyCode;
    }

    /**
     * Sets the value of the deliveredCurrencyCode property.
     * 
     */
    public void setDeliveredCurrencyCode(int value) {
        this.deliveredCurrencyCode = value;
    }

    /**
     * Gets the value of the detailedPurpose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailedPurpose() {
        return detailedPurpose;
    }

    /**
     * Sets the value of the detailedPurpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailedPurpose(String value) {
        this.detailedPurpose = value;
    }

    /**
     * Gets the value of the initiatedFromBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInitiatedFromBranchCode() {
        return initiatedFromBranchCode;
    }

    /**
     * Sets the value of the initiatedFromBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInitiatedFromBranchCode(Integer value) {
        this.initiatedFromBranchCode = value;
    }

    /**
     * Gets the value of the remittanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRemittanceAmount() {
        return remittanceAmount;
    }

    /**
     * Sets the value of the remittanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRemittanceAmount(BigDecimal value) {
        this.remittanceAmount = value;
    }

    /**
     * Gets the value of the remittanceRefNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemittanceRefNumber() {
        return remittanceRefNumber;
    }

    /**
     * Sets the value of the remittanceRefNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemittanceRefNumber(String value) {
        this.remittanceRefNumber = value;
    }

    /**
     * Gets the value of the senderAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderAddress() {
        return senderAddress;
    }

    /**
     * Sets the value of the senderAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderAddress(String value) {
        this.senderAddress = value;
    }

    /**
     * Gets the value of the senderBirthdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSenderBirthdate() {
        return senderBirthdate;
    }

    /**
     * Sets the value of the senderBirthdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSenderBirthdate(XMLGregorianCalendar value) {
        this.senderBirthdate = value;
    }

    /**
     * Gets the value of the senderCountry property.
     * 
     */
    public int getSenderCountry() {
        return senderCountry;
    }

    /**
     * Sets the value of the senderCountry property.
     * 
     */
    public void setSenderCountry(int value) {
        this.senderCountry = value;
    }

    /**
     * Gets the value of the senderCountryOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCountryOfBirth() {
        return senderCountryOfBirth;
    }

    /**
     * Sets the value of the senderCountryOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCountryOfBirth(String value) {
        this.senderCountryOfBirth = value;
    }

    /**
     * Gets the value of the senderMobileNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderMobileNumber() {
        return senderMobileNumber;
    }

    /**
     * Sets the value of the senderMobileNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderMobileNumber(String value) {
        this.senderMobileNumber = value;
    }

    /**
     * Gets the value of the senderName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * Sets the value of the senderName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderName(String value) {
        this.senderName = value;
    }

    /**
     * Gets the value of the senderNationalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderNationalId() {
        return senderNationalId;
    }

    /**
     * Sets the value of the senderNationalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderNationalId(String value) {
        this.senderNationalId = value;
    }

    /**
     * Gets the value of the senderNationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderNationality() {
        return senderNationality;
    }

    /**
     * Sets the value of the senderNationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderNationality(String value) {
        this.senderNationality = value;
    }

    /**
     * Gets the value of the senderPassportNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderPassportNumber() {
        return senderPassportNumber;
    }

    /**
     * Sets the value of the senderPassportNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderPassportNumber(String value) {
        this.senderPassportNumber = value;
    }

    /**
     * Gets the value of the thirdPartyAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThirdPartyAccountNumber() {
        return thirdPartyAccountNumber;
    }

    /**
     * Sets the value of the thirdPartyAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThirdPartyAccountNumber(String value) {
        this.thirdPartyAccountNumber = value;
    }

    /**
     * Gets the value of the thirdPartyBankCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThirdPartyBankCode() {
        return thirdPartyBankCode;
    }

    /**
     * Sets the value of the thirdPartyBankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThirdPartyBankCode(String value) {
        this.thirdPartyBankCode = value;
    }

    /**
     * Gets the value of the thirdPartyBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThirdPartyBranchCode() {
        return thirdPartyBranchCode;
    }

    /**
     * Sets the value of the thirdPartyBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThirdPartyBranchCode(String value) {
        this.thirdPartyBranchCode = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the companyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * Sets the value of the companyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCode(String value) {
        this.companyCode = value;
    }

}

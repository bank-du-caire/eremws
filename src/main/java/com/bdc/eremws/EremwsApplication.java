package com.bdc.eremws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EremwsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EremwsApplication.class, args);
    }

}
